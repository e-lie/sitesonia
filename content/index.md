---
Title: Sonia Marin Academic page
Description: Sonia Marin Academic page
Subtitle: Doctoral Researcher
Year: 2017
Email: sonia.marin@inria.fr
Address: Inria Saclay - Île de France</br>1 rue Honoré d'Estienne d'Orves</br>Bâtiment Alan Turing</br>Campus de l'École Polytechnique</br>91120 Palaiseau
---
##Education

**2014-present**</br>
Ecole Polytechnique, Inria Saclay: PhD, Computer Science, with Dale Miller and Lutz Straßburger
Title : Nested proof theory for modal logics

**2010-2014**</br>
Ecole Normale Supérieure, Paris : School Diploma, Cognitive Science

**2013-2014**</br>
Université Paris Descartes – Ecole Normale Supérieure, Paris : Master Degree, Cognitive Science 
Thesis : Unawareness effect on decision-making with Mikael Cozic and Brian Hill (IHPST)

**2011-2013**</br>
Université Paris Diderot: Master Degree, Mathematical Logic and Foundations of Computer Science
Thesis : Deductive systems for modal logics in nested sequents with Lutz Straßburger (INRIA)

**Spring 2012**</br>
University of Copenhagen : Erasmus exchange, Department of Mathematics

**2010-2011**</br>
Université Pierre et Marie Curie, Paris : Bachelor Degree, Mathematics


##Publications

**August 2016**</br>
A focused framework for emulating modal proof systems. With Dale Miller and Marco Volpe.
Proceedings of AIML 2016.

July 2016
Modular focused systems for intuitionistic modal logics. With Kaustuv Chaudhuri and Lutz Straßburger.
Proceedings of FSCD 2016.

April 2016
Focused and Synthetic Nested Sequents. With Kaustuv Chaudhuri and Lutz Straßburger.
Proceedings of FoSSaCS 2016.

August 2014
Label-free Modular Systems for Classical and Intuitionistic Modal Logics. With Lutz Straßburger. Proceedings of AIML 2014. 



##In preparation

Some theory of indexed nested sequents. With Lutz Straßburger.

Focused proof systems for geometric theories. With Dale Miller and Marco Volpe.



Graduate schools and research visits

October 2016
Visiting Roman Kuznets in Vienna to work on Intuitionistic justification logic, 4 days.

June 2016
Visiting Alessio Guglielmi in Bath to work on Proof search in deep inference, 14 days.

August 2015
European Summer School on Logic, Language and Information, University Pompeu Fabra, Barcelona.

June 2015
Summer school of Topology, Algebra, and Category in Logic, University of Salerno.

April 2015
Midlands Graduate School, University of Sheffield.


##Talks

17 November 2016
Comparing BOX and ! via polarities.
FISP project kick-off meeting, Innsbrück.

10 November 2016
Comparing BOX and ! via polarities.
Linear Logic Workshop, Lyon.

12 October 2016
Focused emulation of modal proof systems.
Theory and Logic group seminar, TU Wien, Vienna.

22 June 2016
Modular focused proof systems for intuitionistic modal logics.
FSCD-1, Porto.

16 June 2016 
Tidy proof systems for intuitionistic modal logics.
Programming, Logic and Semantics group seminar, ITU, Copenhagen.

7 April 2016
Focused nested sequents.
Algebra and Coalgebra in Proof theory, Vienna.

14 December 2015
Focused and synthetic nested sequents.
Workshop on Efficient and Natural Proof Systems, Bath.

13 October 2015
Focusing for nested sequents.
Journées nationales Geocal-Lac-Ltp, Nancy.

22 June 2015
A cut-free proof system for pseudo-transitive modal logics. 
Topology, Algebra, and Categories in Logic 2015, Ischia.

6 August 2014
Label-free modular systems for classical and intuitionistic modal logics.
AIML 2014, Groningen.

13 July 2014
Modular systems for intuitionistic modal logic in nested sequents. 
Workshop on Gentzen Systems and Beyond 3, Vienna Summer of Logic.




##Teaching at UPMC

Spring 2016
1M002 - Suites et intégrales, Algèbre linéaire (1st year)

Fall 2016
1M001 - Analyse et algèbre pour les sciences (1st year)
2M216 - Fonctions de plusieurs variables, analyse vectorielle, intégrales multiples (2nd year)







